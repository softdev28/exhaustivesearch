/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.exhaustivesearch;

/**
 *
 * @author ACER
 */
public class ExhaustiveSearch {

    public static boolean canPartition(int[] n) {
        int s = 0;
        for (int i = 0; i < n.length; i++) {
            s += n[i];
        }
        if (s % 2 != 0) {
            return false;
        }
        return findPartition(n, s / 2, 0);
    }

    private static boolean findPartition(int[] n, int s, int cur) {
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        if (s == 0) {
            return true;
        }
        if (n.length == 0 || cur >= n.length) {
            return false;
        }
        if (n[cur] <= s) {
            if (findPartition(n, s - n[cur], cur + 1)) {
                return true;
            }
        }
        return findPartition(n, s, cur + 1);
    }

    public static void main(String[] args) {
        int[] n = {6, 12, 23, 4, 1};
        System.out.println(canPartition(n));
    }
}
